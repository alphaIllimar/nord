import { IPremission } from "./IPremission";

export interface IPremissionGroup {
    /**
     * The name of the premissions group
     */
    name:string;
    /**
     * Holds all the premissions of the roles in the same order as defined in the IPremissionData.roles array
     */
    premissions:IPremission[];
}