/**
 * Holds the individual premision info
 */
export interface IPremission {
    /**
     * Holds the name of the premission
     */
    name:string;
    /**
     * Holds the description of the premission
     */
    description:string;
    /**
     * Holds the role premisions as in the 
     */
    rolePremmisions:boolean[];
}