import { IPremissionGroup } from "./IPremissionGroup";

/**
 * Holds the premissions data as available on the premisions page
 */
export interface IPremissionData {
    /**
     * The names of the roles.
     * The first one is assumed to be the admin role
     */
    roles:string[];
    /**
     * Holds all the available premission groups
     */
    groups:IPremissionGroup[];
}