import styles from './Sidebar.module.scss';
import { ISidebarProps } from './ISidebarProps'
import Left from './assets/Left.svg?react';
import { Menu } from '../Menu/Menu';

import People from './assets/people.svg?react';
import File from './assets/file.svg?react';
import List from './assets/list.svg?react';
import Flag from './assets/flag.svg?react';
import Risk from './assets/risk_score.svg?react';
import Locked from './assets/locked.svg?react';
import Profile from './assets/profile.svg?react';
import { IState, useState } from '../../react-hooks/useState';

/**
 * Holds all the available pages
 */
export enum Pages {
    /**
     * The empty page needs to be shown
     */
    Empty,
    /**
     * The premissions page
     */
    Premissions,
    Members,
    Billing,
    Questionnaires,
    Docs,
    JurisdictionalRisk,
    RiskAssessments,
    MyProfile
}

/**
 * Renders out the sidebar of the page
 */
export function Sidebar(props:ISidebarProps) {
    /**
     * The page that is selected
     */
    const _seletcedPage:IState<Pages> = useState<Pages>(props.page);
    /**
     * Handles a page change
     * @param page The page to be selected
     */
    const _handlePageChange = (page:Pages) => {
        _seletcedPage.set(page);
        props.onPageChange(page);
    };

    return (
        <div className={styles.container}>
            <div className={styles.backContainer}>
                <div className={styles.back}>
                    <button
                        onClick={() => _handlePageChange(Pages.Empty)}
                    >
                        <Left className={styles.icon} />
                    </button>
                    <div className={styles.text}>Settings</div>
                </div>
            </div>
            <Menu 
                heading='company'
                items={[{
                    text: 'Members',
                    icon: People,
                    value: Pages.Members
                }, {
                    text: 'Billing',
                    icon: File,
                    value: Pages.Billing
                }, {
                    text: 'Questionnaires',
                    icon: List,
                    value: Pages.Questionnaires
                }, {
                    text: 'Docs',
                    icon: File,
                    value: Pages.Docs
                }, {
                    text: 'Jurisdictional risk',
                    icon: Flag,
                    value: Pages.JurisdictionalRisk
                }, {
                    text: 'Risk assessments',
                    icon: Risk,
                    value: Pages.RiskAssessments
                }, {
                    text: 'Permissions',
                    icon: File,
                    value: Pages.Premissions
                }]}
                value={_seletcedPage.value}
                onChange={(page:Pages) => _handlePageChange(page)}
            />
            <Menu 
                heading='personal'
                items={[{
                    text: 'My profile',
                    icon: Profile,
                    value: Pages.MyProfile
                }]}
                value={_seletcedPage.value}
                onChange={(page:Pages) => _handlePageChange(page)}
            />
        </div>
    );
}