import { Pages } from "./Sidebar"

export interface ISidebarProps {
    /**
     * Will be called when the page needs to change
     * @param page The new page to be changed to
     */
    onPageChange:(page:Pages) => void;
    /**
     * The currently selected page
     */
    page:Pages;
}