export interface ITooltipProps {
    /**
     * The text of the tooltip
     */
    text:string;
}