import * as React from 'react';
import { ITooltipProps } from './ITooltipProps';
import styles from './Tooltip.module.scss';
import { IState, useState } from '../../react-hooks/useState';

interface ITooltipPos {
    x:number,
    y:number
}

/**
 * Renders a tooltip to content
 */
export function Tooltip(props:React.PropsWithChildren<ITooltipProps>) {
    const _tipPos:IState<ITooltipPos|null> = useState<ITooltipPos|null>(null);    
    const _textRef:React.MutableRefObject<HTMLDivElement|null> = React.useRef(null);
    const _containerRef:React.MutableRefObject<HTMLDivElement|null> = React.useRef(null);
    
    return (
        <span 
            className={styles.container}   
            ref={_containerRef}
            onMouseLeave={() => _tipPos.set(null)}         
            onMouseMove={(e:React.MouseEvent<HTMLSpanElement, MouseEvent>) => {

                const containerBox:DOMRect = _containerRef.current!.getBoundingClientRect();
                const targetBox:DOMRect = (e.target as any).getBoundingClientRect();
                
                _textRef.current?.style.setProperty('--x', `${(targetBox.left - containerBox.left) - (_textRef.current.offsetWidth / 2) - 5 + e.nativeEvent.offsetX }px`);
                _textRef.current?.style.setProperty('--y', `${(targetBox.top - containerBox.top) - _textRef.current.offsetHeight - 10 + e.nativeEvent.offsetY }px`);
            }}
        >
            {
                props.children
            }            
            <div 
                    className={styles.text}                    
                    ref={_textRef}
                >
                    {props.text}
            </div>
        </span>
    );  
}