export interface IToggleProps {
    /**
     * If TRUE the toggle will be checked
     */
    isChecked:boolean;
    /**
     * The method that will be called when the toggle changes
     * @param value The new value of the toggle
     */
    onChange?:(value:boolean) => void;
    /**
     * If TRUE the toggle will be disabled
     */
    isDisabled?:boolean;
}