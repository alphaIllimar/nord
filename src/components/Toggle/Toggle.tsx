import { IToggleProps } from "./IToggleProps";
import styles from './Toggle.module.scss';

/**
 * Renders the app toggle,
 * Is a controlled element
 */
export function Toggle(props:IToggleProps) {
    return (
        <div className={styles.container}>
            <label>
                <input 
                    onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                        if(!props.isDisabled) {
                            props.onChange?.(e.target.checked);
                        }
                    }}
                    checked={props.isChecked} 
                    type="checkbox" 
                    disabled={props.isDisabled}
                />
                <div className={styles.toggle}>
                    <div className={styles.nob}></div>
                </div>
            </label>
        </div>
    );
}