export interface ISelectProps {
    /**
     * The value of the select
     */
    value:any;
    /**
     * The method that will be called when the selection changes
     * @param newValue The new value that was selected
     */
    onChange:(newValue:any) => void;
    /**
     * The label of the select
     */
    label:string;
    /**
     * The options of the select
     */
    options:ISelectItem[];
}
/**
 * Describes the selection options
 */
export interface ISelectItem {
    /**
     * The value of the select item
     */
    value:any;
    /**
     * The text of the select item
     */
    text:string;
}