import { IState, useState } from "../../react-hooks/useState";
import { ISelectItem, ISelectProps } from "./ISelectProps";
import styles from './Select.module.scss';
import Down from './assets/down.svg?react';
import * as _ from 'lodash';
import * as React from 'react';


/**
 * Renders out a single select selection box
 */
export function Select(props:ISelectProps) {

    const _selectedOption:ISelectItem|undefined = props.options.find((o:ISelectItem) => o.value === props.value);
    const _isOpen:IState<boolean> = useState<boolean>(false);
    const _radioName:string = _.uniqueId('input-radio-');
    const _refFirstOption:React.MutableRefObject<HTMLInputElement | null> = React.useRef<HTMLInputElement|null>(null);
    const _refButton:React.MutableRefObject<HTMLButtonElement | null> = React.useRef<HTMLButtonElement|null>(null);

    const _handleOptionChange = (value:any) => {
        props.onChange(value);
        _isOpen.set(false);
    };

    const _handleSelectKeypress = (e:React.KeyboardEvent<HTMLButtonElement>) => {
        switch(e.code) {
            case 'ArrowDown':
            case 'ArrowUp': {

                e.stopPropagation();
                e.preventDefault();
                e.nativeEvent.stopImmediatePropagation();
                e.nativeEvent.stopPropagation();
                e.nativeEvent.preventDefault();

                const willOpen:boolean = !_isOpen.value;
                if(willOpen && e.code === 'ArrowDown') {                                                            
                    

                    setTimeout(() => {
                        if(_refFirstOption.current) {
                            _refFirstOption.current.focus({
                                preventScroll: true
                            } as FocusOptions);
                            _refFirstOption.current.checked = true;
                        }
                    }, 1);

                    _isOpen.set(willOpen);
                    return false;
                }
                else if(!willOpen && e.code === 'ArrowUp') {                    
                    _isOpen.set(willOpen);
                    return false;
                }
                                
                break
            }
        }
        
    };

    return (
        <div className={styles.container}>
            <div className={styles.label}>{props.label}</div>
            <div className={styles.selectContainer}>
                <div 
                    className={styles.select}
                    onClick={() => {
                        _isOpen.set(!_isOpen.value);
                    }}
                    data-isOpen={_isOpen.value}
                >
                    <button 
                        className={styles.text}
                        onClick={() => _isOpen.set(true)}
                        onKeyUp={_handleSelectKeypress}
                        ref={_refButton}
                    >
                            {_selectedOption?.text}
                    </button>
                    <div className={styles.iconContainer}>
                        <Down className={styles.icon} />
                    </div>
                </div>
                <div className={styles.options}>
                    <ul>
                    {
                        props.options.map((option:ISelectItem, index:number) => (
                            <li 
                                key={index}
                                onClick={(e) => {
                                    if(e.currentTarget.tagName === 'li') {
                                        _handleOptionChange(option.value)
                                    }                                        
                                }}                                    
                            >
                                <label>
                                    <input 
                                        type="radio" 
                                        name={_radioName}
                                        ref={(el:HTMLInputElement) => {
                                            if(!index) {
                                                _refFirstOption.current = el;
                                            }
                                        }}
                                        onChange={() => props.onChange(option.value)}
                                        onKeyUp={(e) => {
                                            if(e.code === 'Enter') {
                                                _isOpen.set(false);
                                                setTimeout(() => {
                                                    _refButton.current?.focus({
                                                        preventScroll: true
                                                    });
                                                }, 1);
                                            }
                                        }}
                                        tabIndex={0}
                                    />
                                    {option.text}
                                </label>
                            </li>
                        ))   
                    }
                    </ul>
                </div>
            </div>
        </div>
    );
}