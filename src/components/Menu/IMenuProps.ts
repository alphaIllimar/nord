import { IMenuItem } from "./IMenuItem";

export interface IMenuProps {
    /**
     * The heading of the menu
     */
    heading:string;
    /**
     * The items to be displayed
     */
    items:IMenuItem[];
    /**
     * The method that will be called when the menu value changes
     * @param selectedValue The new selected value
     */
    onChange:(selectedValue:any) => void;
    /**
     * The currently set value
     */
    value:any;
}