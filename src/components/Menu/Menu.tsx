import { IMenuItem, IconType } from "./IMenuItem";
import { IMenuProps } from "./IMenuProps";
import styles from './Menu.module.scss';

/**
 * Renders out a side menu
 */
export function Menu(props:IMenuProps) {
            
    return (
        <div className={styles.container}>
            <h2>{props.heading}</h2>
            <ul>
                {
                    props.items.map((item:IMenuItem, index:number) => {
                        const Icon:IconType = item.icon;
                        return (
                            <li 
                                className={item.value === props.value ? styles.selected : ''}                                
                                key={index}
                            >
                                <a
                                    onClick={() => {
                                        if(item.value === props.value   ) {
                                            return;
                                        }

                                        props.onChange(item.value);
                                    }}
                                >
                                    <Icon className={styles.icon} />
                                    <div className={styles.text}>{item.text}</div>
                                </a>
                                
                            </li>
                        );
                    })
                }
            </ul>
        </div>
    );
}