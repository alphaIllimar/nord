import * as React from 'react';

export type IconType = React.FunctionComponent<React.SVGProps<SVGSVGElement> & { title?: string | undefined; }>;

/**
 * Holds the single menu item
 */
export interface IMenuItem {
    /**
     * The display text of the item
     */
    text:string;
    /**
     * The value of the menu item
     */
    value:any;
    /**
     * The SVG icon component
     */
    icon:IconType;
}