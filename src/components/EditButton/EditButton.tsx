import { IEditButtonProps } from "./IEditButtonProps";
import Dotts from './assets/dots-vertical.svg?react';
import styles from './IEditButton.module.scss';
import { IState, useState } from "../../react-hooks/useState";
import Trash from './assets/trach.svg?react';
import Pencil from './assets/pencil.svg?react';
import * as React from 'react';


/**
 * Renders out the edit button with the options dropdown
 */
export function EditButton(props:IEditButtonProps) {
    const _isOpen:IState<boolean> = useState<boolean>(false);

    const _handleMenuClose = () => {
        if(_isOpen.value) {
            document.removeEventListener('click', _handleMenuClose);
        }
        _isOpen.set(false);
    };

    const _handleMenuToggle = (e:React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        const isOpen:boolean = !_isOpen.value;
        _isOpen.set(isOpen);
        
        if(!isOpen) {
            _handleMenuClose();
        } else {
            document.addEventListener('click', _handleMenuClose);
        }

        e.stopPropagation();
    };

    React.useEffect(() => {
        return _handleMenuClose;
    }, []);

    return (
        <>
            <div className={styles.container}>
                <button
                    onClick={_handleMenuToggle}
                >
                    <Dotts className={styles.icon} />
                </button>
                {
                    _isOpen.value &&
                    <div 
                        className={styles.menu}
                        onClick={(e:React.MouseEvent<HTMLDivElement, MouseEvent>) => {
                            e.stopPropagation();
                            return false;
                        }}
                    >
                        <div className={styles.edit}>
                            <a
                                onClick={() => {
                                    props.onEdit();
                                    _handleMenuClose();
                                }}
                            >
                                <Pencil className={styles.icon} />
                                <div className={styles.text}>
                                    Edit details
                                </div>
                            </a>
                        </div>
                        <hr />
                        <div className={styles.remove}>
                            <a
                                onClick={() => {
                                    props.onDelete();
                                    _handleMenuClose();
                                }}
                            >
                                <Trash className={styles.icon} />
                                <div className={styles.text}>
                                    Remove
                                </div>
                            </a>
                        </div>
                    </div>
                }                
            </div>
        </>
    );
}