export interface IEditButtonProps {
    /**
     * The method that will be called on edit click
     */
    onEdit:() => void;
    /**
     * The method that will be called on remove/delete click
     */
    onDelete:() => void;
}