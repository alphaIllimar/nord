import * as React from 'react';
import { IPanelProps } from './IPanelProps';
import styles from './Panel.module.scss';
import Close from './assets/close.svg?react';

/**
 * Renders out the side panel
 */
export function Panel(props:React.PropsWithChildren<IPanelProps>) {
    return (
        <div 
            className={`${styles.container} ${props.isOpen ? styles.isOpen : ''}`}
            onClick={props.onClose}
        >
            <div 
                className={styles.panel}
                onClick={(e:React.MouseEvent<HTMLDivElement, MouseEvent>) => {
                    e.stopPropagation();
                    return false;
                }}
            >
                <div className={styles.header}>
                    <h2>{props.headerText}</h2>
                    <div className={styles.closeButtonContainer}>
                        <button
                            onClick={props.onClose}
                        >
                            <Close className={styles.icon} />
                        </button>
                    </div>
                </div>
                <div className={styles.content}>
                    { props.children }
                </div>
                {
                    !!props.footer &&
                    <div className={styles.footer}>
                        { props.footer }
                    </div>
                }
            </div>
        </div>
    );
}