import * as React from 'react';

export interface IPanelProps {
    /**
     * The method that will be called when the panel is closed
     */
    onClose:() => void;
    /**
     * The heading text on the header
     */
    headerText:string;
    /**
     * If TRUE the panel will be opened
     */
    isOpen:boolean;
    /**
     * The footer on the panel
     */
    footer?:React.ReactNode;
}