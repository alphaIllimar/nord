import { ITextInputProps } from "./ITextInputProps";
import styles from './TextInput.module.scss';

/**
 * Renders out a text input field
 */
export function TextInput(props:ITextInputProps) {
    return (
        <label className={styles.container}>
            <div className={styles.label}>Role name</div>
            <input 
                value={props.value}
                onChange={(e) => props.onChange(e.target.value)}
            />
        </label>
    );
}