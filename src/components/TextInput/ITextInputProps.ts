export interface ITextInputProps {
    /**
     * The value of the text input
     */
    value:string;
    /**
     * The method that will be called when the input value changes
     * @param newValue The new value entered into the input     
     */
    onChange:(newValue:string) => void;
}