import { IPremissionData } from '../../models/IPremissionData';
import { IState, useState } from '../../react-hooks/useState';
import { g_service } from '../../services/PremissionsService';
import { EditButton } from '../EditButton/EditButton';
import { Panel } from '../Panel/Panel';
import { Toggle } from '../Toggle/Toggle';
import { Tooltip } from '../Tooltip/Tooltip';
import Plus from './assets/plus.svg?react';
import styles from './Permissions.module.scss';
import * as React from 'react';
import Locked from './assets/locked.svg?react';
import { IPremissionGroup } from '../../models/IPremissionGroup';
import { IPremission } from '../../models/IPremission';
import * as _ from 'lodash';
import { TextInput } from '../TextInput/TextInput';
import { Select } from '../Select/Select';

/**
 * Describes the new role creation object
 */
interface INewRole {
    /**
     * The name of the new role
     */
    name:string;
    /**
     * The role index of the role whom roles will be used as the base roles
     */
    baseRoleIndex:number;
}

/**
 * Describes the values that can be edited for a role
 */
interface IEditRole {
    /**
     * The name of the role
     */
    name:string;
    /**
     * The role index to be edited
     */
    index:number;
}

/**
 * Renders out the premissions page
 */
export function Permissions() {
    /**
     * Holds the premissions data
     */
    const _data:IState<IPremissionData|null> = useState<IPremissionData|null>(null);
    /**
     * Holds the new role information for the new role creation.
     * If NULL then the new role panel is closed
     */
    const _newRole:IState<INewRole|null> = useState<INewRole|null>(null);
    /**
     * Holds the object for the role editing.
     * If NULL then the edit panel is not opened
     */
    const _editRole:IState<IEditRole|null> = useState<IEditRole|null>(null);

    React.useEffect(() => {
        g_service.getData().then((data:IPremissionData) => {
            _data.set(data);
        })
    }, []); 


    /**
     * Renders out the group table rows
     * @param group The group to be rendered
     * @param groupIndex The group index in the data
     * @returns The table section to be added
     */
    const _renderPremissionsGroup = (group:IPremissionGroup, groupIndex:number):React.ReactNode => {

        const headerRow = (
            <tr className={styles.groupName}>
                <th>{group.name}</th>
                {
                    _data.value?.roles.map((name:string, index:number) => (
                        <td key={index}></td>
                    ))
                }
            </tr>
        );

        return (
            <React.Fragment key={groupIndex}>
                { headerRow }
                {
                    group.premissions.map((premission:IPremission, premissionIndex:number) => (
                        <tr key={premissionIndex} className={styles.premissionRow}>
                            <th>
                                <div className={styles.premissionsContainer}>
                                    <div className={styles.name}>{premission.name}</div>
                                    <div className={styles.description}>{premission.description}</div>
                                </div>
                            </th>
                            {
                                premission.rolePremmisions.map((rolePremission:boolean, roleIndex) => (
                                    <td key={roleIndex}>
                                        <div className={styles.toggleContainer}>
                                        {
                                            !!roleIndex ? (
                                                <Toggle 
                                                    isChecked={rolePremission}
                                                    onChange={() => {
                                                        const data:IPremissionData = _.cloneDeep(_data.value!);
                                                        data.groups[groupIndex].premissions[premissionIndex].rolePremmisions[roleIndex] = !rolePremission;
                                                        g_service.saveData(data).then((newData:IPremissionData) => {
                                                            _data.set(newData);
                                                        });
                                                    }}                                            
                                                />
                                            ) : (
                                                <Tooltip text='Admin permissons are not editable'>
                                                    <Toggle 
                                                        isChecked={rolePremission} 
                                                        isDisabled                                          
                                                    />
                                                </Tooltip>
                                            )
                                        }
                                        </div>
                                    </td>
                                ))
                            }
                        </tr>
                    ))
                }
            </React.Fragment>
        );
    };

    /**
     * Creates a new role into the service based of the inputs
     */
    const _createNewRole = () => {
        const data:IPremissionData = _.cloneDeep(_data.value!);
        data.roles.push(_newRole.value!.name);
        data.groups.forEach((group:IPremissionGroup) => {
            group.premissions.forEach((premission:IPremission) => {
                premission.rolePremmisions.push(premission.rolePremmisions[_newRole.value!.baseRoleIndex]);
            });
        });

        g_service.saveData(data).then((newData:IPremissionData) => {
            _data.set(newData);
            _newRole.set(null);
        });
    };
    /**
     * Deletes a role from the service
     * @param roleIndex The role index of the role to be removed
     */
    const _deleteRole = (roleIndex:number) => {
        const data:IPremissionData = _.cloneDeep(_data.value!);
        data.roles.splice(roleIndex, 1);

        data.groups.forEach((group:IPremissionGroup) => {
            group.premissions.forEach((premission:IPremission) => {
                premission.rolePremmisions.splice(roleIndex, 1);
            });
        });

        g_service.saveData(data).then((newData:IPremissionData) => {
            _data.set(newData);
            _newRole.set(null);
        });
    };

    return (
        <div>
            <div className={styles.header}>
                <h1>Premissions</h1>
                <div className={styles.buttonsContainer}>
                    <button
                        onClick={() => _newRole.set({
                            name: '',
                            baseRoleIndex: 0
                        })}
                    >
                        <Plus className={styles.icon} />
                        New role
                    </button>
                </div>
            </div>
            <Panel 
                isOpen={!!_newRole.value}
                headerText='Create new role'
                onClose={() => _newRole.set(null)}
                footer={(
                    <div className={styles.panelButtons}>
                        <button
                            onClick={() => _newRole.set(null)}
                        >
                            Cancel
                        </button>
                        <button
                            onClick={() => _createNewRole()}
                            className={styles.panelSaveButton}
                        >
                            Create role
                        </button>
                    </div>
                )}
            >
                <TextInput 
                    value={_newRole.value?.name || ''}
                    onChange={(newValue:string) => {
                        const role:INewRole = _.cloneDeep(_newRole.value!);
                        role.name = newValue;
                        _newRole.set(role);
                    }}
                />
                <Select
                    options={(_data.value?.roles || []).map((name:string, index:number) => ({
                        text: name,
                        value: index
                    }))}
                    onChange={(newIndex:number) => {
                        const role:INewRole = _.cloneDeep(_newRole.value!);
                        role.baseRoleIndex = newIndex;
                        _newRole.set(role);
                    }}
                    label='Exisiting roles to inherit permissions from'
                    value={_newRole.value?.baseRoleIndex}
                />
            </Panel>
            <Panel 
                isOpen={!!_editRole.value}
                headerText='Edit'
                onClose={() => _editRole.set(null)}
                footer={(
                    <div className={styles.panelButtons}>
                        <button
                            onClick={() => _editRole.set(null)}
                        >
                            Cancel
                        </button>
                        <button
                            onClick={() => {
                                const data:IPremissionData = _.cloneDeep(_data.value!);
                                data.roles[_editRole.value!.index] = _editRole.value!.name;

                                g_service.saveData(data).then((newData:IPremissionData) => {
                                    _data.set(newData);
                                    _editRole.set(null);
                                });
                            }}
                            className={styles.panelSaveButton}
                        >
                            Save
                        </button>
                    </div>
                )}
            >
                <TextInput 
                    value={_editRole.value?.name || ''}
                    onChange={(newValue:string) => {
                        const role:IEditRole = _.cloneDeep(_editRole.value!);
                        role.name = newValue;
                        _editRole.set(role);
                    }}
                />                
            </Panel>
            <table>
                <thead>
                    <tr>
                        <td>Actions</td>
                        {
                            _data.value?.roles.map((roleName:string, index:number) => {
                                return (
                                    <th key={index}>
                                        {
                                            index ? (
                                                <div className={styles.normal}>
                                                    <div className={styles.text}>{ roleName }</div>
                                                    <EditButton 
                                                        onDelete={() => _deleteRole(index)}
                                                        onEdit={() => {
                                                            _editRole.set({
                                                                index,
                                                                name: roleName
                                                            });
                                                        }}
                                                    />
                                                </div>
                                            ) : (
                                                <div className={styles.admin}>
                                                    {roleName} <Locked className={styles.icon}/>
                                                </div>
                                            )
                                        }
                                    </th>
                                )
                            })
                        }
                    </tr>
                </thead>    
                <tbody>
                {
                    _data.value?.groups.map((g:IPremissionGroup, index:number) => _renderPremissionsGroup(g, index))
                }
                </tbody>            
            </table>
        </div>
    );
}