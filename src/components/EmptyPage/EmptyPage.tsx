/**
 * Renders out the empty page component
 */
export function EmptyPage() {
    return <div></div>;
}