import styles from './App.module.scss';
import { EmptyPage } from './components/EmptyPage/EmptyPage';
import { Permissions } from './components/Permissions/Permissions';
import { Pages, Sidebar } from './components/Sidebar/Sidebar';
import { IState, useState } from './react-hooks/useState';
import * as React from 'react';

export function App() {
    /**
     * Holds the currently selected page
     */
    const _selectedPage:IState<Pages> = useState<Pages>(Pages.Premissions);
    /**
     * Renders out a given page
     * @param page The page to be rendered
     * @returns The page node
     */
    const _renderPage = (page:Pages):React.ReactNode => {
        switch(page) {
            case Pages.Premissions:
                return <Permissions />;
            default:
                return <EmptyPage />;
        }
    }

    return (
        <div className={styles.container}>
            <Sidebar 
                onPageChange={(page:Pages) => {
                    _selectedPage.set(page);
                }}
                page={_selectedPage.value}
            />
            <div className={styles.pageContainer}>
                { _renderPage(_selectedPage.value) }
            </div>            
        </div>
    );
}
