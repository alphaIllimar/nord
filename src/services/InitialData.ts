import { IPremissionData } from "../models/IPremissionData";

/**
 * Holds all the data the app will load in on initial load if there is no pre-existing data
 */
export const InitialData:IPremissionData = {
    roles: [
        'Admin',
        'Member',
        'Manager',
        'Limited Managers'
    ],
    groups: [
        {
            name: 'General',
            premissions: [
                {
                    name: 'Add client',
                    description: 'The ability to add and edit clients.',
                    rolePremmisions: [
                        true, true, true, false
                    ]
                },
                {
                    name: 'Delete client',
                    description: 'The ability to delete clients.',
                    rolePremmisions: [
                        true, false, false, false
                    ]
                },
                {
                    name: 'Add involvement',
                    description: 'The ability to add, edit and delete involvements.',
                    rolePremmisions: [
                        true, false, false, false
                    ]
                }
            ]
        },
        {
            name: 'Team member access',
            premissions: [
                {
                    name: 'Set permissions and access role',
                    description: 'The ability to update permissions and change access roles.',
                    rolePremmisions: [
                        true, false, true, false
                    ]
                }
            ]
        },
        {
            name: 'Billing',
            premissions: [
                {
                    name: 'Edit billing details',
                    description: 'View and edit company billing details.',
                    rolePremmisions: [
                        true, false, false, false
                    ]
                }
            ]
        },
        {
            name: 'Screening',
            premissions: [
                {
                    name: 'Ongoing monitoring',
                    description: 'The ability to turn ongoing monitoring on or off.',
                    rolePremmisions: [
                        true, false, true, false
                    ]
                },
                {
                    name: 'Add hit',
                    description: 'The ability to add and edit hits.',
                    rolePremmisions: [
                        true, false, true, false
                    ]
                },
                {
                    name: 'Remove hit',
                    description: 'The ability to delete hits.',
                    rolePremmisions: [
                        true, false, true, false
                    ]
                }
            ]
        },
        {
            name: 'Questionnaires and Risk Assessments',
            premissions: [
                {
                    name: 'Add field',
                    description: 'Add and edit fields in the questionnaire and risk assessment builder.',
                    rolePremmisions: [
                        true, false, false, false
                    ]
                },
                {
                    name: 'Customer risk assessment',
                    description: 'The ability to access the risk assessment settings.',
                    rolePremmisions: [
                        true, false, false, false
                    ]
                },
                {
                    name: 'Jurisdictional risk assessment',
                    description: 'The ability to view and edit the jurisdictional risk assessment.',
                    rolePremmisions: [
                        true, false, false, false
                    ]
                }
            ]
        }
    ]
};

