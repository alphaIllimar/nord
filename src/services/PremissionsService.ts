import { IPremissionData } from "../models/IPremissionData";
import { InitialData } from "./InitialData";

/**
 * The key for the localstorage data
 */
const DATA_KEY:string = 'premissions-data';

/**
 * The localstorage version for the premissions service
 */
export class PremissionsService {
    /**
     * Returns all the data of the app
     */
    public async getData():Promise<IPremissionData> {
        const strData:string|null = localStorage.getItem(DATA_KEY);
        const data:IPremissionData = strData ? JSON.parse(strData) : InitialData;        
        return Promise.resolve(data);
    }
    /**
     * Saves a new set of data into the local storage
     * @param newData The new data to be saved
     * @returns The new saved instance
     */
    public async saveData(newData:IPremissionData):Promise<IPremissionData> {
        const strData:string = JSON.stringify(newData);
        localStorage.setItem(DATA_KEY, strData);
        return JSON.parse(strData);
    }
}

/**
 * The service instance that the app uses
 */
export const g_service:PremissionsService = new PremissionsService();